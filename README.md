# Puppet-Python3

Puppet-Python3 is a Puppet module that manages installation of python3 and python 3 
virtual environments.  The module uses the venv package, so assumes a version of python 
providing it.

## class  python3

### Parameters
`Boolean $dev=false`: If true, the python3-dev package is installed.

`Hash $venvs={}`: A hash of python3::venv resources.


## define python3::venv

A defined resource representing a virtual environment

### Parameters
`$title`: the path at which to create the virtual environment.

The next five parameters correspond to flags passed to pyvenv.  If undef, then pyvenv 
uses its defaults.

`Optional[Boolean] $system_site_packages=undef`:

`Optional[Boolean] $symlinks=undef`:

`Optional[Boolean] $copies=undef`:

`Optional[Boolean] $clear=undef`:

`Optional[Boolean] $upgrade=undef`:
`Optional[Boolean] $without_pip=undef`:

`Array $requirements=[]`:  A list of requirements.txt entries, to be created in the
virtual environment directory.

`Hash $vpkgs={}`: a hash of virtual package resource data representing package resources to be realized.
`Hash $files={}`: a hash of file resource data representing other file resources to be created.

