class python3(Boolean $dev=false, Hash $venvs={})
    {
    package{'python3': ensure => installed}
    
    if $dev
        {
        package{'python3-dev': ensure => installed}
        }
        
    create_resources(python3::venv, $venvs)
    }