# $title: path at which to create virtual environment.
define python3::venv
    (
    String $owner,
    String $group,
    Optional[Boolean] $system_site_packages=undef,
    Optional[Boolean] $symlinks=undef, 
    Optional[Boolean] $copies=undef, 
    Optional[Boolean] $clear=undef, 
    Optional[Boolean] $upgrade=undef, 
    Optional[Boolean] $without_pip=undef, 
    Array[String] $requirements=[],
    Hash $vpkgs={},
    Hash $files={},
    )
    {
    realize Package['python3-venv']
    create_resources('@package', $vpkgs, {ensure => present}) 
        
    $pyenv_args = template('python3/pyenv_args.erb')
    
    exec
        {
        "make_pyvenv_$title":
        command => "pyvenv$pyenv_args $title && chown -R $owner:$group '$title'",
        path => $path,
        unless => "test -d $title",
        }~>
    exec
        {
        "upgrade_pip":
        command => "$title/bin/pip --upgrade pip",
        refreshonly => true,
        }->  
    file
        {
        "$title/requirements.txt":
        ensure => file, 
        owner => "$owner", 
        group => "$group",
        mode => '0644',
        content => template('python3/requirements.txt.erb'),
        }~>
    exec
        {
        "$title/bin/pip install --upgrade --requirement $title/requirements.txt":
        refreshonly => true,
        }
    
    create_resources(file, $files, {'owner' => "$user", 'group' => "$group"})
    }
